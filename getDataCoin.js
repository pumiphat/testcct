const axios = require('axios');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const dayjs = require("dayjs")
const path = require("path")
const createCsvStringifier = require('csv-writer').createObjectCsvStringifier;
const fs = require('fs');
const iconv = require('iconv-lite');
const moment = require('moment');
const inquirer = require('inquirer');

const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();

let startDate = moment("2020-01-01");
let dateNow = moment().toDate();


async function call(){

    inquirer
    let answer = await inquirer.prompt([
        {
            name: 'getCoinName',
            message: 'What coin you want to get data?',
            type: 'input',
        },
    ]);
    // console.log(answer);
    let result = await CoinGeckoClient.coins.fetchMarketChartRange(`${answer.getCoinName}`, {
        vs_currency: "thb",
        from: startDate.valueOf()/1000,
        to: dateNow/1000,
      });
    
    return result
        // answer : answer
    
}

async function marketValue() {
    
    try {
        let result = await call()
        const diffDays = Math.ceil(
            Math.abs(dateNow - startDate) / (1000 * 60 * 60 * 24)
          );
        // console.log(result.data.prices)
        let pathFile = `${path.join(`${__dirname}/getDataCoin`)}.csv`
        const csvWriter = createCsvStringifier({
            header: [
                { id: "date", title: "date" },
                { id: "prices", title: "Prices" },
                { id: "market_caps", title: "Market Caps" },
                { id: "total_volumes", title: "Total Volumes" },
            ],
        })
        let records = [
            
        ];
        for (let i = 0; i < diffDays; i++) {
            records.push({
                date: moment(Number.parseFloat(result.data.prices[i][0])).format('DD/MM/YYYY'),  
                prices:Number.parseFloat(result.data.prices[i][1]).toLocaleString('en-US', {minimumFractionDigits: 2}) , 
                market_caps:Number.parseFloat(result.data.market_caps[i][1]).toLocaleString('en-US', {minimumFractionDigits: 2}) , 
                total_volumes:Number.parseFloat(result.data.total_volumes[i][1]).toLocaleString('en-US', {minimumFractionDigits: 2})
            },)
        }
        // console.log(records);
        const headerLine = csvWriter.getHeaderString()
        const recordLines = csvWriter.stringifyRecords(records)
        const toShiftJIS = (text) => iconv.encode(text, "utf8")
        fs.writeFileSync(pathFile, toShiftJIS("\ufeff" + headerLine + recordLines))
        
    } catch (err) {
        console.log(err)
    }
}

marketValue()