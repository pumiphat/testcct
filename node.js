const axios = require('axios');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const dayjs = require("dayjs")
const path = require("path")
const createCsvStringifier = require('csv-writer').createObjectCsvStringifier;
const fs = require('fs');
const iconv = require('iconv-lite');

async function marketValue() {
    try {
        let resultBtc = await axios.get('https://api.coingecko.com/api/v3/coins/bitcoin/history?date=01-01-2020');
        let resultEth = await axios.get('https://api.coingecko.com/api/v3/coins/ethereum/history?date=01-01-2020');
    
        // let stringRandom = randomstring.generate({
        //     length: 6,
        //     charset: "alphanumeric",
        // })
        let pathFile = `${path.join(`${__dirname}/MarketValue`)}.csv`
        const csvWriter = createCsvStringifier({
            header: [
                { id: "id", title: "Coin" },
                { id: "symbol", title: "Symbol" },
                { id: "name", title: "Name"},
                { id: "market_data", title: "Price" },
                { id: "market_cap", title: "Market Cap" },
                { id: "total_volume", title: "Total Volume" },
            ],
        })
        const records = [
            {
                id: resultBtc.data.id,  
                symbol: resultBtc.data.symbol, 
                name: resultBtc.data.name , 
                market_data:Number.parseFloat(resultBtc.data.market_data.current_price.thb).toLocaleString('en-US', {minimumFractionDigits: 2}) , 
                market_cap:Number.parseFloat(resultBtc.data.market_data.market_cap.thb).toLocaleString('en-US', {minimumFractionDigits: 2}) , 
                total_volume:Number.parseFloat(resultBtc.data.market_data.total_volume.thb).toLocaleString('en-US', {minimumFractionDigits: 2})
            },
            {
                id: resultEth.data.id,  
                symbol: resultEth.data.symbol, 
                name: resultEth.data.name , 
                market_data:Number.parseFloat(resultEth.data.market_data.current_price.thb).toLocaleString('en-US', {minimumFractionDigits: 2}) , 
                market_cap:Number.parseFloat(resultEth.data.market_data.market_cap.thb).toLocaleString('en-US', {minimumFractionDigits: 2}) , 
                total_volume:Number.parseFloat(resultEth.data.market_data.total_volume.thb).toLocaleString('en-US', {minimumFractionDigits: 2})
            },
        ];
        const headerLine = csvWriter.getHeaderString()
        const recordLines = csvWriter.stringifyRecords(records)
        const toShiftJIS = (text) => iconv.encode(text, "utf8")
        fs.writeFileSync(pathFile, toShiftJIS("\ufeff" + headerLine + recordLines))
        
        let dataReponse = []
        fs.createReadStream(pathFile)
            .on("data", (row) => {
                dataReponse.push(row)
            })
            .on("end", () => {
            })
    } catch (err) {
        console.log(err)
        // response.setResponse(`${serviceName} - Error`, 500, err)
    }
}

marketValue()